const copyToClipboard = str => {
    const el = document.createElement('textarea');
    el.value = JSON.stringify(str);
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    alert('json copied to clipboard');
};

const downloadForm = () => {

    const form = document.getElementById('downloadForm');
    form.submit();
};

const showPassword = () => {

    var key_attr = $('#key').attr('type');
    if(key_attr != 'text') {
        $('#key').attr('type', 'text');
    } else {
        $('#key').attr('type', 'password');
    }
}

$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('div.alert').delay(5000).slideUp(300);
});
