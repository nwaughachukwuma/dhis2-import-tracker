<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\File;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Storage;
use App\Upload;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        $uploadedFile = $request->file('file');

        if ( !$uploadedFile) {
            return back()->with(['status'=>'Choose a file to upload', 'alert_type'=>'info']);
        }

        // ensure file size is <= 256kb
        $fileSize = $uploadedFile->getClientSize() / 1024;
        if ($fileSize > 1024*256) {
            return back()->with(['status'=>"File size is: $filesize kb. Contact admin for support with large files > 256kb"]);
        }
        $filename = time().$uploadedFile->getClientOriginalName();
        $extension = $uploadedFile->getClientOriginalExtension();
        if ($extension != 'csv') {
            return back()->with(['status'=>'Please upload csv only']);
        }

        $storedFile = Storage::disk('public')->putFileAs(
            'public/csv', $uploadedFile, $filename
        );

        $upload = new Upload;
        $upload->filename = $filename;
        $upload->filepath = $storedFile;
        $upload->user()->associate(auth()->user());
        $upload->save();

        // return response()->json(['message' => $filename], 200);
        $fileName = $uploadedFile->getClientOriginalName();
        $realFileName = $filename;

        $disk = config('filesystems.default', 'local');
        $rowCount = $this->getFileCount($filename, $upload->filepath, $disk);
        // get the file size
        // $size = Storage::disk('s3')->size("public/csv/$filename") / 1024;
        $size = round($fileSize, 2);
        return view('convert_to_json', compact('realFileName', 'fileName', 'rowCount', 'size' ));
    }

    private function getFileCount($file, $filepath=null, $disk=null, $delimiter=',') {

        if ($disk=='ftp') {
            $file = "https://storage.hbruw.com.ng/$filepath";
        }else if ($disk == 'local' || $disk == 'public'){
            $file = public_path("storage/csv/$file");
        }else { // s3
            $adapter = Storage::disk('s3')->getAdapter();
            $client = $adapter->getClient();
            $client->registerStreamWrapper();
            $object = $client->headObject([
                'Bucket' => $adapter->getBucket(),
                'Key' => /*$adapter->getPathPrefix() . */$filepath,
            ]);

            $file = "s3://{$adapter->getBucket()}/{$filepath}";
        }
        // $file = Storage::url("public/$file/$file"); // public_path("storage/$file/$file");

        if (($handle = fopen($file, 'r')) === false) {
            die('Error opening file');
        }
        $headers = fgetcsv($handle, 1024, $delimiter, '"');
        $rowCount = 1;
	    while ($row = fgetcsv($handle, 1024, $delimiter, '"')) {
          $csv2json[] = array_combine($headers, $row);
          $rowCount++;
	    }
        fclose($handle);
	    return $rowCount;
    }

    public function convertToJson(Request $request) {

        $filename = $request->input('fileName');
        $filepath = "public/csv/$filename";
        $disk = config('filesystems.default', 'local');

        if ($disk == 'ftp') {
            $filename = "https://storage.hbruw.com.ng/public/csv/$filename";
        }else if ($disk == 'local' || $disk == 'public') {
            $filename = public_path("storage/csv/$filename");
        }else {
            // s3
            //$filename = "s3://dhis2-import-tracker/public/csv/$filename"; // s3
            $adapter = Storage::disk('s3')->getAdapter();
            $client = $adapter->getClient();
            $client->registerStreamWrapper();
            $object = $client->headObject([
                'Bucket' => $adapter->getBucket(),
                'Key' => $filepath,
            ]);
            $filename = "s3://{$adapter->getBucket()}/{$filepath}";
        }

        $json = array();
        try{
            if (($handle = fopen($filename, "r")) !== FALSE) {
                $rownum = 0;
                $header = array();
                while (($row = fgetcsv($handle, 1024, ",")) !== FALSE) {
                    if ($rownum === 0) {
                        for($i=0; $i < count($row); $i++){
                            $header[$i] = trim($row[$i]);
                        }
                    } else {
                        if (count($row) === count($header)) {
                            $rowJson = array();
                            $attributes = array();
                            $enrollments = array();
                            $attr_assoc = array();
                            $enrol_assoc =  ['orgUnit'=> $row[1] ];
                            foreach($header as $i=>$head) {
                                if ($i < 2){
                                    $rowJson[$head] = trim($row[$i]);
                                }else{
                                    if ($head == 'attribute' || $head == 'value'){

                                        $attr_assoc[$head] = preg_replace('/\s+/', ' ', $row[$i]);
                                        if ($i % 2 > 0) {
                                            array_push($attributes, $attr_assoc);
                                        }

                                    }else{
                                        if ($head == 'enrollmentDate') {
                                            $enrol_assoc[$head] = date("Y-m-d", strtotime(trim($row[$i])));
                                        }else{
                                            $enrol_assoc[$head] = trim($row[$i]);
                                        }
                                        if ($i % 2 > 0) {
                                            array_push($enrollments, $enrol_assoc);
                                        }
                                    }
                                }
                            }
                            $rowJson['attributes'] = $attributes;
                            $rowJson['enrollments'] = $enrollments;
                            array_push($json, $rowJson);
                        }
                    }
                    $rownum++;
                }
                fclose($handle);
            }
        }catch(Exception $e) {
            return back()->with(['status'=>$e->getMessage(), 'alert_type' => 'danger']);
        }
        $innerVals = json_encode(['trackedEntityInstances' => $json], JSON_UNESCAPED_SLASHES);
        $json_data = $innerVals;
        // return response()->json($json_data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        $fileName = time(). 'converted_file.json';
        Storage::disk($disk)
        ->put("public/json/$fileName", $innerVals); // save the json file
        return view('success_page', compact('json_data', 'fileName'));
    }
    // ref-1: https://wogan.blog/2017/01/04/use-amazon-s3-with-laravel-5/
    // ref-2: https://gist.github.com/fideloper/6ada632650d8677ba23963ab4eae6b48
    public function downloadJson(Request $request) {

        $filename = $request->input('fileName');
        $disk = config('filesystems.default', 'local');

        header("Content-Description: File Transfer");
        header("Content-Type: application/json");

        if ($disk == 'ftp') { // ftp
            $file = "https://storage.hbruw.com.ng/public/json/$filename";
        }else if ($disk == 'local' || $disk == 'public') {
            $file = public_path("storage/json/$filename");
        }else {
            // s3
            $filepath = "public/json/$filename";
            $adapter = Storage::disk('s3')->getAdapter();
            $client = $adapter->getClient();
            $client->registerStreamWrapper();
            $object = $client->headObject([
                'Bucket' => $adapter->getBucket(),
                'Key' => $filepath,
            ]);
            $file = "s3://{$adapter->getBucket()}/{$filepath}";
            header('Last-Modified: '.$object['LastModified']);
            header('Accept-Ranges: '.$object['AcceptRanges']);
            header('Content-Length: '.$object['ContentLength']);
            header('Content-Type: '.$object['ContentType']);
        }

        $assetPath = $file;

        header('Content-Disposition: attachment; filename='.$filename);
        header("Content-Transfer-Encoding: Binary");
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        // header("Cache-Control: public");
        // header("Content-Description: File Transfer");
        // header("Content-Disposition: attachment; filename=" . basename($assetPath));
        // header("Content-Type: application/json");
        return readfile($assetPath);

        // return Storage::disk('s3')->download($assetPath, 'converted_json.json', $headers);
        // return response()->download($file, 'converted_json.json', $headers);
    }

    public function SendToDHIS2(Request $request) {

        $data = $request->all();
        $url = $data['url'];
        $username = $data['username'];
        $password = $data['key'];
        $fileName = $data['fileName'];

        $curlResponse = $this->setUpCurl($url, $username, $password, $fileName);

        // return $curlResponse;

        if ($curlResponse[0] === 'error') {
            if ($curlResponse[1] == 403) {
                $msg = 'Unauthorized action: wrong DHIS2 url or username or password. Please check again and enter correct values';
                abort(403, $msg );
                // return back()
                //     ->with(['status'=>'Wrong dhis2 url or username or password. Please check again and enter correct values',
                //                  'alert_type'=>'warning']);
            }
            $msg = 'Clean your data and confirm your tracked entity types key is correct';
            abort(500, $msg);
        }else {
            return redirect()->route('upload.home')
                    ->with(['status'=>'File successfully uploaded to your DHIS2 instance tracker capture App',
                            'alert_type' => 'success']); // response()->json([$curlResponse[1]], 200);
        }

    }

    private function setUpCurl($url, $username, $password, $filename){

        $fN = public_path("storage/json/$filename");
        $json_data = json_decode(file_get_contents($fN)); // extract json from file

        $base_uri = $url;
        $end_point = '/api/trackedEntityInstances';
        $url = $base_uri . $end_point;

        // declare guzzle http client
        try{
            $client = new Client;
            $response = $client->request('POST', $url, [
                'auth' => [$username, $password],
                'json' => $json_data, // send json data
                'headers' =>array(
                    'Content-Type => application/json', //multipart/form-data',
                    'Accept' => 'application/json',
                    "Connection" => "Keep-Alive"
                )
            ]);
            $status_code = $response->getStatusCode();
            if ($status_code != 200) {
                return ['error', $status_code];
            } else {
                return ['success', $response];
            }
        }catch(GuzzleException $e) {
            return ['error', 403];
        }
    }
}

