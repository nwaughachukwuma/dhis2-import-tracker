<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Telescope extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dhis2-IT:telescope';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monitoring server activities with telescope';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $schedule->command('telescope:prune --hours=48')->daily();
    }
}
