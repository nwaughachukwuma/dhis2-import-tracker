<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware'=>'auth'], function() {
    Route::view('/csv_to_json', 'conversion')->name('upload.home');
    Route::post('/csv_to_json/upload', 'FileController@upload')->name('file.upload');
    Route::post('/csv_to_json/convert', 'FileController@convertToJson')->name('file.convert');
    Route::post('/converted/download', 'FileController@downloadJson')->name('converted.download');
    Route::post('/converted/post_to_dhis2', 'FileController@SendToDHIS2')->name('converted.postdhis2');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/disclaimer', function () {
    return view('disclaimer');
});
