<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>DHIS2 Import Tracker</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                background-image: linear-gradient(to right, #1d5288, #4a749f, #7797b7) !important;
                /*color: #636b6f; */
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .bottom-right {
                position: absolute;
                right: 10px;
                bottom: 5px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: white; /*#636b6f;*/
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .decorate-link {
                text-decoration: underline;
                color: white
            }
            .decorate-link:hover {
                color: tomato;
                display:block;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height" style="height: 96vh">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    DHIS2 Import Tracker
                </div>
                <div>
                    <span style="font-size: 17px">
                        Convert your csv to json and upload to your DHIS2 instance
                    </span>
                </div>

                <div class="links decorate-link">
                    <a href="{{url('csv_to_json')}}">Goto CSV to json converter</a>
                </div>

                <div class="links decorate-link" style="margin-top: 20px">
                    <a href="{{url('disclaimer')}}">Disclaimer</a>
                </div>
            </div>
        </div>
        <div class="footer">
            <label>&copy;2019</label>
            <label>
                <a target="_blank" href="https://peeragehouse.com" style="color: white">Peerage House</a>
            </label>
            <label class="bottom-right">Made with
                <label style="color: red">&#9829;</label>
                by
                <a target="_blank" href="https://linkedin.com/in/cpnwaugha" style="color: white">
                    &scy;&tshcy;&mu;&kappa;&omega;&mu;m&alpha; &Nu;&omega;&alpha;&mu;g&tshcy;&alpha;
                </a>
            </label>
        </div>
    </body>
</html>
