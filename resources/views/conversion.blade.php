@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            {{--  @if (session('status'))
                <div id="session_msg" class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif  --}}
            @if (session('status'))
                <div id="session_msg" class="alert alert-{{session('alert_type')}} alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ session('status') }}</strong>
                </div>
            @endif
            <div class="card">
                <div class="card-header bg-info" style="color: #fff">
                    <b>Upload CSV</b>
                </div>
                <div class="card-body">
                    <span class="content">
                        <p style="font-size: 12; color: red">Click button below for sample csv format</p>
                        <div class="links" style="color: blue!important">
                            <a class='btn btn-primary btn-cust' target="_blank" href="{{url('https://docs.google.com/spreadsheets/d/1XXvGpdS_E_3Ui99VfCnoKhP-aDZmuZr2L7GO2hbxb4c/edit?usp=sharing')}}">
                                CSV format
                            </a>
                        </div>
                    </span>
                    <hr/>
                    <form id="upload_form" method="POST" enctype='multipart/form-data' action="{{ route('file.upload') }}" aria-label="{{ __('Upload') }}">
                        @csrf
                        <div class="form-group row ">
                            <label for="title" class="col-sm-4 col-form-label text-md-right">{{ __('File Upload') }}</label>
                            <div class="col-md-6">
                                <input type="file" id="file" name="file" accept=".csv" />
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button id="submit" type="submit" class="btn btn-primary">
                                    {{ __('Upload File') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


