@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div id="session_msg" class="alert alert-{{session('alert_type')}} alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ session('status') }}</strong>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4" style="font-size: 18px; margin: 5px;">
                            <strong>Converted JSON file</strong>
                        </div>
                        <div class="col-md-7">
                            <span style="margin: 5px;">
                                <i class="fa fa-files-o fa-2x"
                                    style="margin-left: 3px; margin-right: 3px; cursor: pointer"
                                    title="copy to clipboard"
                                    aria-hidden="true"
                                    onclick="copyToClipboard({{$json_data}})">
                                </i>
                                <i class="fa fa-download fa-2x"
                                    style="margin-left: 3px; margin-right: 3px; cursor: pointer"
                                    title="download json"
                                    aria-hidden="true"
                                    onclick="downloadForm()">
                                </i>
                                <a title="Upload a new CSV" href="{{url('csv_to_json')}}">
                                    <i title="upload a new csv" class="fa fa-chevron-left fa-2x" style="margin-left: 3px; margin-right: 3px">
                                    </i>
                                </a>
                            </span>
                            <span style="margin: 5px">
                                <a href="https://jsonformatter.org" target="_blank">
                                    Go here to view formatted json >>
                                </a>
                            </span>
                        </div>
                        <div class="col-md-1" title="download" hidden>
                            <form id="downloadForm" style="margin-top: -15px; padding: 0" method="POST" action="{{ route('converted.download') }}" aria-label="{{ __('Upload') }}">
                                @csrf
                                <div class="form-group row ">
                                    <input type="text" value="{{$fileName}}" name="fileName" hidden />
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-download"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{ str_limit($json_data, 2000) }}
                </div>
                <div class="card-footer">
                    <div class="links">
                        <a class="btn btn-default" href="{{url('/')}}">Go Home</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header" style="background-color: #1d5288; color: #fff; font-size: 18px; margin: 5px;">
                    <strong>
                        Send to your DHIS2 instance
                    </strong>
                </div>
                <div class="card-body">
                    <form id="post_dhis2" method="POST" action="{{route('converted.postdhis2')}}" aria-label="{{ __('POST To DHIS2') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="sr-only">DHIS2 URL</label>
                            <input type="text" name="url" id="url" class="form-control" placeholder="e.g. http://138.201.162.131:8080">
                        </div>
                        <div class="form-group">
                            <label for="key" class="sr-only">Username</label>
                            <input type="username" name="username" id="username" class="form-control" placeholder="e.g. admin" required>
                        </div>
                        <div class="form-group">
                            <label for="key" class="sr-only">Password</label>
                            <input type="password" name="key" id="key" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" onclick="showPassword()">
                            <label class="form-check-label" for="exampleCheck1">Show password</label>
                          </div>
                        <input type="text" value="{{$fileName}}" name="fileName" hidden />
                        <input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" style="background-color: #1d5288; color: #fff" value="POST">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
