@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session()->get('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header bg-info" style="color: #fff">
                    <b>Convert your csv file to JSON</b>
                </div>
                <div class="card-body">
                    <span class="content">
                        <p>
                            Your file name is: {{$fileName}}<br/>
                            Number of rows are: {{$rowCount}}<br/>
                            File size is: {{$size}}kb
                        </p>
                    </span>
                    <hr/>
                    <form method="POST" action="{{ route('file.convert') }}" aria-label="{{ __('Upload') }}">
                        @csrf
                        <div class="form-group row ">
                            <input type="text" value="{{$realFileName}}" name="fileName" hidden />
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Convert to JSON') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="links">
                        <a href="{{url('/')}}">Go Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
