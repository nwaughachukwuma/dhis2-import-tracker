@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info" style="color: #fff">
                    <h3 class="content" style="align-items: 'center'">
                        <b>Disclaimer</b>
                    </h3>
                </div>
                <div class="card-body">
                    <span class="content">
                        <p>Last updated: January 10, 2019</p>
                        <p>
                            The feature presented here on dhis2.hbruw.com.ng website (the "Service") is a result of a simple research made
                            by our team at <a target="_blank" href="https://peeragehouse.com">Peerage House</a> with the aim of reducing the time and tools
                            required to clean data, convert to the appropriate JSON format, before uploading to a DHIS2 instance.
                        </p>

                        <p>
                            The use of this tool has increased our productivity making it possible for us to spend time on other things.
                            Seeing that a lot of requests have been made around this feature, we decided to make it available for general
                            consumption.
                        </p>

                        <p>
                            DHIS2 import tracker is meant for testing and not production ready, so we ask that users make use of test/sample
                            data. Please be reminded that you are not required to enter data sensitive to you or your organization as
                            Peerage House assumes no responsibility for leaked information resulting from the data you test the platform with.
                        </p>

                        <p>
                            The platform also provides feature to import converted data into your DHIS2 instance without having to use your
                            your terminal with import codes. All it requires is the URL, username and password of your instance, and on one
                            click the converted data is sent to your instance. To this end, we strongly warn against using production DHIS2
                            instance for this. You can setup a test instance to try this out.
                        </p>

                        <p>
                            Our goal is to both open-source the solution for any interested person or organization to use within their own
                            watch and control and assist any organization who might need help setting it up.
                        </p>

                        <p>
                            In no event shall Peerage House be liable for any special, direct, indirect, consequential, or incidental damages
                            or any damages whatsoever, whether in an action of contract, negligence or other tort, arising out of or in
                            connection with the use of this Service (DHIS2 import tracker). Peerage House reserves the right to make additions,
                            deletions, or modification to the contents and features on the Service at any time without prior notice.
                        </p>

                        <p>
                            Finally, note that we are still in test phase, and do not guarantee the accuracy or completeness of the converted data
                        </p>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


